.PHONY : bootstrap

bootstrap : bootstrap/kustomization.yaml
	kubectl kustomize --load-restrictor LoadRestrictionsNone bootstrap | kubectl create -f -
