#

## `apps`

A collection of applications to deploy on k8s clusters.

Each folder is a kustomize base, with any upstream vendor manifests provisioned under `vendor/` by `vendir`.

## `bootstrap`

A special kustomize base to bootstrap a new cluster with.
This deploys argocd and calico, using the kustomize bases under `apps`.

The `Makefile` provides `make bootstrap` to facilitate deploying the manifests.
If doing so by hand, ensure you pass `--load-restrictor LoadRestrictionsNone` to either `kubectl kustomize` or `kustomize build`.

